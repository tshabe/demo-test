<?php

namespace App\Http\Controllers;

use App\Models\Station;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StationController extends Controller
{
    public function index()
    {
        $station = Station::latest()->paginate(5);

        return view('stations.index', compact('station'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        $station = DB::table('companies as c')
            ->leftjoin('parentcompanies as p', 'c.parent_company_id', '=', 'p.id')
            ->select('p.id', 'p.name', 'c.parent_company_id')
            ->distinct()
            ->whereNotNull('c.parent_company_id',)
            ->get();

        return view('stations.create', compact('station'));
    }
    public function store(Request $request)
    {
        request()->validate(Station::$rules);

        $station = Station::create($request->all());

        return redirect()->route('stations.index')
            ->with('success', 'Station created successfully.');
    }



    public function edit($id)
    {
        $station = Station::find($id);

        return view('stations.edit', compact('station'));
    }
    public function update(Request $request, Station $station)
    {
        request()->validate(Station::$rules);

        $station->update($request->all());

        return redirect()->route('stations.index')
            ->with('success', 'Station updated successfully');
    }
    public function destroy($id)
    {
        $station = Station::find($id)->delete();

        return redirect()->route('stations.index')
            ->with('success', 'Station deleted successfully');
    }
    public function show($id)
    {
        $station = Station::find($id);

        return view('stations.show', compact('station'));
    }
}
/*$latitude = "23.033863";
     $longitude = "72.585022";
     $users = User::select("name", \DB::raw("6371 * acos(cos(radians(" . $latitude . "))
             * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $longitude . "))
             + sin(radians(" .$latitude. ")) * sin(radians(latitude))) AS distance"))
             ->having('distance', '<', 1000)
             ->orderBy('distance')
             ->get()->toArray();*/