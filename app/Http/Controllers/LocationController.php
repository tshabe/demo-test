<?php

namespace App\Http\Controllers;

use App\Models\Station;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
class LocationController extends Controller
{
    // ---------------- [ Load View ] ----------------
    public function index(Request $request)
    {
     $latitude = "23.033863";
        $longitude = "72.585022";
        $distance = Station::select("name", DB::raw("6371 * acos(cos(radians(" . $latitude . "))
                * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $longitude . "))
                + sin(radians(" .$latitude. ")) * sin(radians(latitude))) AS distance"))
               // ->having('distance')
                ->orderBy('distance')
       
                ->get()->toArray();
                dd($distance);
            
    }
    
}
