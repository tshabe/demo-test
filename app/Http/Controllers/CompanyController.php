<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Models\Parentcompany;

class CompanyController extends Controller
{
    public function index()
    {
        $company = Company::latest()->paginate(5);
        return view('companies.index', compact('company'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }


    public function create()
    {

        $companies = Parentcompany::all();
        return view('companies.create', compact('companies'));
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'parent_company_id' => 'required',
        ]);

        Company::create($request->all());

        return redirect()->route('companies.index')
            ->with('success', 'Child company created successfully.');
    }

    public function show(Company $company)
    {
        return view('companies.show', compact('company'));
    }

    public function edit(Company $company)
    {
        if (request('eager')) {
            $companies = Parentcompany::with('companies')->get();
        } elseif (request('big')) {
            $companies = Parentcompany::with('companies')->has('companies', '>=', 3)->get();
        } else {
            $companies = Parentcompany::all();
        }
        return view('companies.edit', compact('company'));
    }
    public function update(Request $request, Company $company)
    {
        $request->validate([
            'name' => 'required',
            'parent_company_id' => 'required',
        ]);

        $company->update($request->all());

        return redirect()->route('companies.index')
            ->with('success', 'Child company updated successfully');
    }
    public function destroy(Company $company)
    {
        $company->delete();

        return redirect()->route('companies.index')
            ->with('success', 'Child company deleted successfully');
    }
}
