<?php

namespace App\Http\Controllers;

use App\Models\Parentcompany;
use Illuminate\Http\Request;

class ParentcompanyController extends Controller
{
    public function index()
    {
        if (request('eager')) {
            $companies = Parentcompany::with('companies')->get();
        } elseif (request('big')) {
            $companies = Parentcompany::with('companies')->has('companies', '>=', 3)->get();
        } else {
            $companies = Parentcompany::all();
        }
        return view('parentcompanies.index', compact('companies'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        $companies = Parentcompany::all();
        return view('parentcompanies.create', compact('companies'));
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'address' => 'required',
        ]);

        Parentcompany::create($request->all());

        return redirect()->route('parentcompanies.index')
            ->with('success', 'Parent company created successfully.');
    }

    public function show(Parentcompany $parentcompany)
    {
        return view('parentcompanies.show', compact('parentcompany'));
    }

    public function edit(Parentcompany $parentcompany)
    {
        return view('parentcompanies.edit', compact('parentcompany'));
    }
    public function update(Request $request, Parentcompany $parentcompany)
    {
        $request->validate([
            'name' => 'required',
            'address' => 'required',
        ]);

        $parentcompany->update($request->all());

        return redirect()->route('parentcompanies.index')
            ->with('success', 'parent company updated successfully');
    }
    public function destroy(Parentcompany $parentcompany)
    {
        $parentcompany->delete();

        return redirect()->route('parentcompanies.index')
            ->with('success', 'Parent company deleted successfully');
    }
}
