<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Parentcompany extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'address',
    ];
    public function companies()
    {
        return $this->hasMany('App\Models\Company', 'parent_company_id', 'id');
    }
}
