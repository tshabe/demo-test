<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'parent_company_id',
    ];
    public function parentcompany()
    {
        return $this->hasOne('App\Models\Parentcompany', 'id', 'parent_company_id');
    }
    public function stations()
    {
        return $this->hasMany('App\Models\Station', 'company_id', 'id');
    }
}
