<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
  use HasFactory;
  protected $fillable = ['name', 'latitude', 'longitude', 'company_id', 'address'];

  static $rules = [
    'name' => 'required',
    'latitude' => 'required',
    'longitude' => 'required',
    'company_id' => 'required',
    'address' => 'required',
  ];

  protected $perPage = 20;
  public function company()
  {
    return $this->hasMany('App\Models\Company', 'id', 'company_id');
  }
  public function Parents()
  {
    return $this->hasMany('App\Models\Parentcompany', 'id', 'company_id');
  }
}
