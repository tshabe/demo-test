@extends('stations.layout')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New child companies</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('companies.index') }}"> Back</a>
        </div>
    </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
    <strong>Failed!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form action="{{ route('companies.store') }}" method="POST">
    @csrf

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Company Name:</strong>

                <select require class="form-select" aria-label="Default select example" name="name" id ="name">
                    <option selected>Open this select menu</option>
                    
                  
                        @foreach ($companies as $parent)
                       
                        <option value="{{$parent->name}}">{{$parent->name}}</option>
                        
                        @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Owned by Company:</strong>
                <select class="form-select" aria-label="Default select example" name="parent_company_id">
                    <option selected>Open this select menu</option>
                    
                  
                        @foreach ($companies as $pr_c)
                       
                        <option value="{{$pr_c->id}}">{{$pr_c->name}}</option>
                        
                        @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>

</form>
@endsection