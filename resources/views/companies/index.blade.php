@extends('stations.layout')
 
@section('content')
</br>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h5>Child Company details</h5>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('companies.create') }}"> Create New Parent Company</a>
            </div>
        </div>
    </div>
</br>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Company Name</th>
            <th>Parent Company ID</th>
            <th width="280px">Action</th>
        </tr>
  
            
        @foreach ($company as $pr_c)

        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $pr_c->name }}</td>
            <td><b>Code({{ $pr_c->parent_company_id }}): {{ $pr_c->parentcompany->name }} </b></td>
            <td>
                <form action="{{ route('companies.destroy',$pr_c->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('companies.show',$pr_c->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('companies.edit',$pr_c->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    
    </table>
  
    {!! $company->links() !!}
      
@endsection