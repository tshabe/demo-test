@extends('stations.layout')
  
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New Station companies</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('stations.index') }}"> Back</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Failed!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('stations.store') }}" method="POST">
    @csrf
  
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Station Name:</strong>
                <input type="text" name="name" class="form-control" placeholder="Station Name">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Latitude:</strong>
                <input type="text" name="latitude" class="form-control" placeholder="Latitude Values">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Longitude:</strong>
                <input type="text" name="longitude" class="form-control" placeholder="Longitude">
            </div>
        </div>


        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            <strong>Company ID:</strong>
            <select name="company_id" id="company_id" class="form-control input-lg dynamic" data-dependent="city">
           
            @foreach ($station as $parent)
                       
                       <option value="{{$parent->name}}">{{$parent->name}}</option>
                       
                       @endforeach
         
            </select>
            </div>
        </div>



  
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Sation Address:</strong>
                <textarea class="form-control" style="height:150px" name="address" placeholder="Station Address"></textarea>
            </div>
            {{ csrf_field() }}
        </div>
     

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
   
</form>



@endsection