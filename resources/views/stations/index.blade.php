@extends('stations.layout')
 
@section('content')
</br>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h5>Station details</h5>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('stations.create') }}"> Create New station</a>
            </div>
        </div>
    </div></br>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Station Name</th>
            <th>Latitude</th>
            <th>Longitude</th>
            <th>Company ID</th>
            <th>Address</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($station as $pr_c)
       
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $pr_c->name }}</td>
            <td>{{ $pr_c->latitude }}</td>
            <td>{{ $pr_c->longitude }}</td>
            @if ($loop->iteration >=1)
            <td><b>{{ $pr_c->company_id}} </b></td>
            @endif
            <td>{{ $pr_c->address}}</td>
            <td>
                <form action="{{ route('stations.destroy',$pr_c->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('stations.show',$pr_c->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('stations.edit',$pr_c->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>

        @endforeach

        
    </table>
    {!! $station->links() !!}
@endsection