@extends('stations.layout')
 
@section('content')
</br>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h5>Parent Company details</h5>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('parentcompanies.create') }}"> Create New Parent Company</a>
            </div>
        </div>
    </div>
</br>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th> Parent Companies</th>
            <th >Parent Addresses</th>
            <th>Number of Child companies</th>
            <th> List of Child companies</th>
            <th width="280px" >Action</th>
        </tr>
    
        @foreach ($companies as $pr_c)
            @foreach ($pr_c->companies as $child)
        <tr>
            
            @if ($loop->iteration ==1)
            <td>{{ ++$i }}</td>
            <td >{{$pr_c->name }}</td>
            @endif
            @if ($loop->iteration > 1)
            <td></td>
            @endif
            @if ($loop->iteration ==1)
            <td rowspan = "{{ $pr_c->companies->count() }}">{{$pr_c->address}}</td>
            @endif
          
            @if ($loop->iteration > 1)
            <td></td>
            @endif
            <td > @if ($loop->iteration == 1)
                <b>{{ $pr_c->name }}: {{ $pr_c->companies->count() }} company/ies</b>
                @endif
            </td>
            <td>{{ $child->name }}</td>
            <td>
                <form action="{{ route('parentcompanies.destroy',$pr_c->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('parentcompanies.show',$pr_c->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('parentcompanies.edit',$pr_c->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
        @endforeach
    </table>
  
 
      
@endsection