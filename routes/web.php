<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ParentcompanyController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\StationController;
use App\Http\Controllers\LocationController;

Route::resource('parentcompanies', ParentcompanyController::class);
Route::resource('companies', CompanyController::class);
Route::resource('stations', StationController::class);

Route::get('near-places', [LocationController::class, 'index']);

Route::get('/', function () {
    return view('welcome');
});
