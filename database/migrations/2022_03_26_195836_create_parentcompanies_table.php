<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('parentcompanies', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->timestamps();
        });
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('parent_company_id');
            $table->timestamps();

            $table->foreign('parent_company_id')
            ->references('id')
            ->on('parentcompanies')
            ->onDelete('cascade');
        });
        Schema::create('stations', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->double('latitude');
            $table->double('longitude');
            $table->unsignedBigInteger('company_id');
            $table->string('address');
            $table->timestamps();

            
            $table->foreign('company_id')
                ->references('id')
                ->on('companies')
                ->onDelete('cascade');
        });

    }
    public function down()
    {
        
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('parentcompanies');
        Schema::dropIfExists('companies');
        Schema::dropIfExists('stations');
        Schema::enableForeignKeyConstraints();
    }
};
